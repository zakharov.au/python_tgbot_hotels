from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot
from utils.hotel_description import get_description
from utils.get_photo import get_hotel_photo
from keyboards.inline.hotels_key import hotel_keyboard
from keyboards.inline.photo_key import get_photo_keyboard
from utils.get_hotels import hotel_search
from database.database import photo_db_add


# @dp.callback_query_handler(Text(startswith='hotel'), state=UserInfoState.search)
async def hotels(callback_query: types.CallbackQuery, state: FSMContext):
    result = callback_query.data.split('_')[1]
    if result == 'next':
        async with state.proxy() as data:
            hotel_list = data['hotel_list']
            page = data['page']
            data['num_hotel'] += 1
            num_hotel = data['num_hotel']
        if num_hotel + 1 < len(hotel_list):
            description = await get_description(hotel_list, num_hotel)
            hotel_key = await hotel_keyboard(hotel_list, num_hotel, page)
            await callback_query.message.delete()
            await bot.send_photo(callback_query.from_user.id, caption=description, photo=hotel_list[num_hotel]['Thumb'], parse_mode='HTML', reply_markup=hotel_key)
        else:
            async with state.proxy() as data:
                info = data
                data['page'] += 1
                page = data['page']
                data['num_hotel'] = 0
                command = data['command']
            hotel_list = await hotel_search(info, command, page)
            async with state.proxy() as data:
                data['hotel_list'] = hotel_list
            description = await get_description(hotel_list, 0)
            hotel_key = await hotel_keyboard(hotel_list, 0, page)
            await callback_query.message.delete()
            await bot.send_photo(callback_query.from_user.id, caption=description, photo=hotel_list[0]['Thumb'],
                                 parse_mode='HTML', reply_markup=hotel_key)

    elif result == 'back':
        async with state.proxy() as data:
            hotel_list = data['hotel_list']
            page = data['page']
            data['num_hotel'] -= 1
            num_hotel = data['num_hotel']
        if num_hotel < 0:
            async with state.proxy() as data:
                info = data
                data['page'] -= 1
                page = data['page']
                command = data['command']
            hotel_list = await hotel_search(info, command, page)
            async with state.proxy() as data:
                data['hotel_list'] = hotel_list
                num_hotel = len(hotel_list) - 1
                data['num_hotel'] = num_hotel
            description = await get_description(hotel_list, num_hotel)
            hotel_key = await hotel_keyboard(hotel_list, num_hotel, page)
            await callback_query.message.delete()
            await bot.send_photo(callback_query.from_user.id, caption=description, photo=hotel_list[num_hotel]['Thumb'],
                                 parse_mode='HTML', reply_markup=hotel_key)

        else:
            description = await get_description(hotel_list, num_hotel)
            hotel_key = await hotel_keyboard(hotel_list, num_hotel, page)
            await callback_query.message.delete()
            await bot.send_photo(callback_query.from_user.id, caption=description, photo=hotel_list[num_hotel]['Thumb'], parse_mode='HTML', reply_markup=hotel_key)

    elif result == 'foto':
        async with state.proxy() as data:
            hotel_list = data['hotel_list']
            num_hotel = data['num_hotel']
            data['num_photo'] = 0
            num_photo = data['num_photo']
        hotel_id = hotel_list[num_hotel]['hotel_id']
        async with state.proxy() as data:
            data['last_hotel_id'] = hotel_id
        photo_list = await get_hotel_photo(hotel_id)
        async with state.proxy() as data:
            data['photo_list'] = photo_list
        photo_keyboard = await get_photo_keyboard(num_photo, len(photo_list))
        await bot.send_photo(callback_query.from_user.id, photo=photo_list[num_photo],
                             reply_markup=photo_keyboard)


# @dp.callback_query_handler(Text(startswith='photo'), state=UserInfoState.search)
async def hotel_foto(callback_query: types.CallbackQuery, state: FSMContext):
    result = callback_query.data.split('_')[1]
    if result == 'next':
        async with state.proxy() as data:
            photo_list = data['photo_list']
            data['num_photo'] += 1
            num_photo = data['num_photo']
            user_id = data['user_id']
            search_id = data['search_id']
            last_hotel_id = data['last_hotel_id']
        photo_keyboard = await get_photo_keyboard(num_photo, len(photo_list))
        await callback_query.message.delete()
        await photo_db_add(user_id, search_id, last_hotel_id, photo_list[num_photo])
        await bot.send_photo(callback_query.from_user.id, photo=photo_list[num_photo],
                             reply_markup=photo_keyboard)
    elif result == 'back':
        async with state.proxy() as data:
            photo_list = data['photo_list']
            data['num_photo'] -= 1
            num_photo = data['num_photo']
        photo_keyboard = await get_photo_keyboard(num_photo, len(photo_list))
        await callback_query.message.delete()
        await bot.send_photo(callback_query.from_user.id, photo=photo_list[num_photo],
                             reply_markup=photo_keyboard)
    elif result == 'cancel':
        await callback_query.message.delete()
