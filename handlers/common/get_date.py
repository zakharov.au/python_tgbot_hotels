from keyboards.inline.date_key import years_keyboard, yes_no_keyboard_date, month_keyboard, days_keyboard
from states.user_state import UserInfoState
from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot


# @dp.callback_query_handler(Text(startswith='startyear'), state=UserInfoState.start_year)
async def start_year(callback_query: types.CallbackQuery, state: FSMContext):
    start_year = callback_query.data.split('_')[1]
    async with state.proxy() as data:
        data['start_year'] = start_year
    month_keybrd = await month_keyboard('start', start_year)
    await UserInfoState.start_month.set()
    await callback_query.message.delete()
    await bot.send_message(callback_query.from_user.id, text='📆 Выберите месяц начала аренды:',
                           reply_markup=month_keybrd)


# @dp.callback_query_handler(Text(startswith='startmonth'), state=UserInfoState.start_month)
async def start_month(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'startmonth_year_change':
        await UserInfoState.start_year.set()
        year_keyboard = await years_keyboard('start')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте год начала аренды:',
                               reply_markup=year_keyboard)
    else:
        start_month = callback_query.data.split('_')[1]
        async with state.proxy() as data:
            data['start_month'] = start_month
            start_year = data['start_year']
        day_keybrd = await days_keyboard('start', start_year, start_month)
        await UserInfoState.start_day.set()
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Выберите день начала аренды:',
                               reply_markup=day_keybrd)


# @dp.callback_query_handler(Text(startswith='startday'), state=UserInfoState.start_day)
async def start_day(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'startday_year':
        await UserInfoState.start_year.set()
        year_keyboard = await years_keyboard('start')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте год начала аренды:',
                               reply_markup=year_keyboard)
    elif callback_query.data == 'startday_month':
        await UserInfoState.start_month.set()
        async with state.proxy() as data:
            start_year = data['start_year']
        months_keyboard = await month_keyboard('start', start_year)
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте месяц начала аренды:',
                               reply_markup=months_keyboard)
    else:
        start_day = callback_query.data.split('_')[1]
        async with state.proxy() as data:
            data['start_day'] = start_day
            start_year = data['start_year']
            start_month = data['start_month']
        yes_no_keybrd = await yes_no_keyboard_date('start')
        await UserInfoState.start_check.set()
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id,
                               text='📅 Дата начала аренды {}-{}-{}. Верно?'.format(start_year, start_month, start_day),
                               reply_markup=yes_no_keybrd)


#@dp.callback_query_handler(Text(startswith='startcheck'), state=UserInfoState.start_check)
async def start_check(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'startcheck_yes':
        await UserInfoState.end_year.set()
        year_keyboard = await years_keyboard('end')
        await callback_query.message.delete()
        async with state.proxy() as data:
            start_day = data['start_day']
            start_year = data['start_year']
            start_month = data['start_month']
        await bot.send_message(callback_query.from_user.id,
                               text='📅 Выбранная дата начала аренды {}-{}-{}.'.format(start_year, start_month, start_day))
        await bot.send_message(callback_query.from_user.id, text='📆 Укажите год завершения аренды:',
                               reply_markup=year_keyboard)
    elif callback_query.data == 'startcheck_no':
        await UserInfoState.start_year.set()
        year_keyboard = await years_keyboard('start')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Укажите год начала аренды:',
                               reply_markup=year_keyboard)


# @dp.callback_query_handler(Text(startswith='endyear'), state=UserInfoState.end_year)
async def end_year(callback_query: types.CallbackQuery, state: FSMContext):
    end_year = callback_query.data.split('_')[1]
    async with state.proxy() as data:
        data['end_year'] = end_year
    month_keybrd = await month_keyboard('end', end_year)
    await UserInfoState.end_month.set()
    await callback_query.message.delete()
    await bot.send_message(callback_query.from_user.id, text='📆 Выберите месяц завершения аренды:',
                           reply_markup=month_keybrd)


# @dp.callback_query_handler(Text(startswith='endmonth'), state=UserInfoState.end_month)
async def end_month(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'endmonth_year_change':
        await UserInfoState.end_year.set()
        year_keyboard = await years_keyboard('end')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте год завершения аренды:',
                               reply_markup=year_keyboard)
    else:
        end_month = callback_query.data.split('_')[1]
        async with state.proxy() as data:
            data['end_month'] = end_month
            end_year = data['end_year']
        day_keybrd = await days_keyboard('end', end_year, end_month)
        await UserInfoState.end_day.set()
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Выберите день завершения аренды:',
                               reply_markup=day_keybrd)


# @dp.callback_query_handler(Text(startswith='endday'), state=UserInfoState.end_day)
async def end_day(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'endday_year':
        await UserInfoState.end_year.set()
        year_keyboard = await years_keyboard('end')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте год завершения аренды:',
                               reply_markup=year_keyboard)
    elif callback_query.data == 'endday_month':
        await UserInfoState.end_month.set()
        async with state.proxy() as data:
            end_year = data['end_year']
        months_keyboard = await month_keyboard('end', end_year)
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='📆 Поменяйте месяц завершения аренды:',
                               reply_markup=months_keyboard)
    else:
        end_day = callback_query.data.split('_')[1]
        async with state.proxy() as data:
            data['end_day'] = end_day
            end_year = data['end_year']
            end_month = data['end_month']
        yes_no_keybrd = await yes_no_keyboard_date('end')
        await UserInfoState.end_check.set()
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id,
                               text='📅 Дата завершения аренды {}-{}-{}. Верно?'.format(end_year, end_month, end_day),
                               reply_markup=yes_no_keybrd)


