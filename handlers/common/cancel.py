from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot
from keyboards.reply.menu import menu_keyboard, menu_cancel


# @dp.message_handler(text='❌ Отмена', state='*')
async def cancel(message: types.Message, state: FSMContext):
    await message.delete()
    await state.finish()
    await bot.send_message(message.from_user.id, 'Отменено', reply_markup=menu_keyboard)
