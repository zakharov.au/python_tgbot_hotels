from keyboards.inline.hotels_key import hotel_keyboard
from utils.get_hotels import hotel_search
from utils.hotel_description import get_description
from states.user_state import UserInfoState
from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot
from utils.get_command_text import get_city_search_text
from database.database import add_db_data


# @dp.callback_query_handler(Text(startswith='final'), state=UserInfoState.check)
async def check(callback_query: types.CallbackQuery, state: FSMContext):
    result = callback_query.data.split('_')[1]
    if result == 'yes':
        mess_id = await bot.send_message(callback_query.from_user.id, 'Выполняется поиск....')
        async with state.proxy() as data:
            info = data
            command = data['command']
        hotel_list = await hotel_search(info, command, 1)
        user_id = callback_query.from_user.id
        await add_db_data(info)
        if hotel_list == [0]:
            await UserInfoState.start.set()
            city_search_text = await get_city_search_text(command)
            await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id['message_id'])
            mess_id = await bot.send_message(callback_query.from_user.id, 'На данные даты ничего не найдено. Попробуем еще раз. '
                                                                '{}'.format(city_search_text))
            async with state.proxy() as data:
                data['mess_id'] = mess_id['message_id']
        else:
            async with state.proxy() as data:
                data['hotel_list'] = hotel_list
                data['page'] = 1
                data['num_hotel'] = 0
                page = data['page']
                data['mess_id'] = mess_id['message_id']
            await callback_query.message.delete()
            description = await get_description(hotel_list, 0)
            hotel_key = await hotel_keyboard(hotel_list, 0, page)
            await UserInfoState.search.set()
            await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id['message_id'])
            await bot.send_photo(callback_query.from_user.id, caption=description, photo=hotel_list[0]['Thumb'], parse_mode='HTML', reply_markup=hotel_key)
    elif result == 'no':
        await UserInfoState.start.set()
        async with state.proxy() as data:
            command = data['command']
            city_search_text = await get_city_search_text(command)
        await bot.send_message(callback_query.from_user.id, 'Попробуем еще раз. '
                                                            '{}'.format(city_search_text))
