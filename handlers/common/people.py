from states.user_state import UserInfoState
from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot
from keyboards.inline.pearson_key import adult_num_keyboard, child_num_keyboard, age_child_keyboard
from keyboards.inline.date_key import years_keyboard
from keyboards.inline.yes_no_key import yes_no_keyboard_final
from utils.book_description import get_book_description


# @dp.callback_query_handler(Text(startswith='endcheck'), state=UserInfoState.end_check)
async def end_check(callback_query: types.CallbackQuery, state: FSMContext):
    if callback_query.data == 'endcheck_yes':
        await UserInfoState.adults.set()
        await callback_query.message.delete()
        adults_num_keyboard = await adult_num_keyboard()
        async with state.proxy() as data:
            end_day = data['end_day']
            end_year = data['end_year']
            end_month = data['end_month']
        await bot.send_message(callback_query.from_user.id,
                               text='📅 Выбранная дата завершения аренды {}-{}-{}.'.format(end_year, end_month, end_day))
        await bot.send_message(callback_query.from_user.id, text='🧑👨 Укажите количество проживающих взрослых:',
                               reply_markup=adults_num_keyboard)
    elif callback_query.data == 'endcheck_no':
        await UserInfoState.end_year.set()
        year_keyboard = await years_keyboard('end')
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id, text='Укажите год завершения аренды:',
                               reply_markup=year_keyboard)


# @dp.callback_query_handler(Text(startswith='adult'), state=UserInfoState.adults)
async def adult_num(callback_query: types.CallbackQuery, state: FSMContext):
    adults = callback_query.data.split('_')[1]
    async with state.proxy() as data:
        data['adults'] = adults
    await UserInfoState.child.set()
    child_keyboard = await child_num_keyboard()
    await callback_query.message.delete()
    await bot.send_message(callback_query.from_user.id, text='👧‍👦  Укажите количество проживающих детей:',
                           reply_markup=child_keyboard)


# @dp.callback_query_handler(Text(startswith='childnum'), state=UserInfoState.child)
async def child_worker(callback_query: types.CallbackQuery, state: FSMContext):
    childs = callback_query.data.split('_')[1]
    async with state.proxy() as data:
        data['child'] = childs
        data['num_age'] = 0
    if int(childs) > 0:
        async with state.proxy() as data:
            data['num_age'] += 1
            num_age = data['num_age']
            data['agechilds'] = list()
        age_keyboard = await age_child_keyboard(num_age)
        await UserInfoState.agechilds.set()
        await callback_query.message.delete()
        await bot.send_message(callback_query.from_user.id,
                               text='‍👦 Укажите возраст {} ребенка:'.format(num_age),
                               reply_markup=age_keyboard)

    else:
        await callback_query.message.delete()
        async with state.proxy() as data:
            data_d = data
        text = await get_book_description(data_d)
        yes_no_keyboard_fin = await yes_no_keyboard_final()
        await UserInfoState.check.set()
        await bot.send_message(callback_query.from_user.id, text, reply_markup=yes_no_keyboard_fin, parse_mode='HTML')


# @dp.callback_query_handler(Text(startswith='child_age'), state=UserInfoState.agechilds)
async def age_worker(callback_query: types.CallbackQuery, state: FSMContext):
    age = callback_query.data.split('_')[3]
    async with state.proxy() as data:
        childs = int(data['child'])
        num_age = data['num_age']
        data['agechilds'].append(age)
        ages = data['agechilds']
    await callback_query.message.delete()
    if childs > num_age:
        async with state.proxy() as data:
            data['num_age'] += 1
            num_age = data['num_age']
        age_keyboard = await age_child_keyboard(num_age)

        await bot.send_message(callback_query.from_user.id,
                               text='‍👦 Укажите возраст {} ребенка:'.format(num_age),
                               reply_markup=age_keyboard)
    else:
        async with state.proxy() as data:
            data_d = data
        text = await get_book_description(data_d)
        yes_no_keyboard_fin = await yes_no_keyboard_final()
        await UserInfoState.check.set()
        await bot.send_message(callback_query.from_user.id, text, reply_markup=yes_no_keyboard_fin, parse_mode='HTML')

