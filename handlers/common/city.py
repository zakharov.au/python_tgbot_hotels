from states.user_state import UserInfoState
from aiogram.dispatcher import FSMContext
from aiogram import types
from loader import bot
from keyboards.inline.city_key import get_city_keyboard
from keyboards.inline.date_key import years_keyboard
from utils.get_city import city_search
from utils.get_command_text import get_city_search_text, get_type_search_text
from keyboards.reply.menu import menu_cancel


# @dp.message_handler(state=UserInfoState.start)
async def get_city(message: types.Message, state: FSMContext):
    city = message.text
    status, city_dict = await city_search(city)
    if status:
        async with state.proxy() as data:
            data['city_dict'] = city_dict
        city_keyboard = await get_city_keyboard(city_dict)
        await UserInfoState.city.set()
        async with state.proxy() as data:
            mess_id = data['mess_id']
        await bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
        await bot.delete_message(chat_id=message.chat.id, message_id=mess_id)
        await bot.send_message(message.from_user.id, '🏙 Уточните город?', reply_markup=city_keyboard)
    else:
        async with state.proxy() as data:
            command = data['command']
        city_search_text = await get_city_search_text(command)
        await bot.send_message(message.from_user.id,
                               '🏙 Город не найден. Попробуйте еще раз. {}'.format(city_search_text),
                               reply_markup=menu_cancel)


# @dp.callback_query_handler(Text(startswith='city_'), state=UserInfoState.city)
async def set_city(callback_query: types.CallbackQuery, state: FSMContext):
    city_num = int(callback_query.data.split('_')[1])
    async with state.proxy() as data:
        city_dict = data['city_dict']
    city = city_dict[city_num][0]
    city_id = city_dict[city_num][1]
    async with state.proxy() as data:
        data['city'] = city
        data['city_id'] = city_id
        command = data['command']
    year_keyboard = await years_keyboard('start')
    await callback_query.message.delete()
    await bot.send_message(callback_query.from_user.id, '🏙 Выбранный город - {}'.format(city))
    if command == 'lowprice' or command == 'highprice':
        await UserInfoState.start_year.set()
        await bot.send_message(callback_query.from_user.id, text='📆 Выберите год начала аренды:',
                               reply_markup=year_keyboard)
    elif command == 'bestdeal':
        await UserInfoState.distance.set()
        mess_id = await bot.send_message(callback_query.from_user.id, text='🚶 Укажите максимально допустимое расстояние от отеля '
                                                                 'до центра города в километрах:')
        async with state.proxy() as data:
            data['mess_id'] = mess_id['message_id']
