from loader import bot
from aiogram import types
from aiogram.dispatcher import Dispatcher


# @dp.message_handler(commands='start', state=None)
async def echo(message: types.Message):
    await bot.send_message(message.from_user.id, 'Для начала поиска введите команду /start')


def register_handlers_echo(dp: Dispatcher):
    dp.register_message_handler(echo, state=None)
