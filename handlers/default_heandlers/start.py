from loader import bot, dp
from aiogram import types
from aiogram.dispatcher import Dispatcher
from keyboards.reply.menu import menu_keyboard
from config_data.config import DEFAULT_COMMANDS


# @dp.message_handler(commands='start', state=None)
async def start(message: types.Message):
    await bot.send_message(message.from_user.id, 'Добро пожаловать в бот поиска отелей! Выберите тип поиска в нижнем меню! ⬇️', reply_markup=menu_keyboard)
    await dp.bot.set_my_commands(DEFAULT_COMMANDS)


def register_handlers_start(dp: Dispatcher):
    dp.register_message_handler(start, commands='start', state=None)
