from loader import bot
from aiogram import types
from aiogram.dispatcher import Dispatcher, FSMContext
from states.user_state import UserInfoState
from keyboards.reply.menu import menu_cancel
from aiogram.dispatcher.filters import Text
from utils.get_command_text import get_city_search_text, get_type_search_text
from handlers.common.get_date import start_year, start_month, start_day, start_check, end_year, end_month, end_day
from handlers.common.cancel import cancel
from handlers.common.people import end_check, adult_num, child_worker, age_worker
from handlers.common.hotels import hotels, hotel_foto
from handlers.common.city import get_city, set_city
from handlers.common.final_check import check
from database.database import get_new_searchid


# @dp.message_handler(text='💰 Дорогие отели', state=None)
# @dp.message_handler(commands=['highprice', 'Бюджетные отели'], state=None)
async def highprice_start(message: types.Message, state: FSMContext):
    command = 'highprice'
    user_id = message.from_user.id
    search_id, time = await get_new_searchid()
    async with state.proxy() as data:
        data['command'] = command
        data['user_id'] = user_id
        data['time'] = time
        data['search_id'] = search_id
    await UserInfoState.start.set()
    type_search_text = await get_type_search_text(command)
    city_search_text = await get_city_search_text(command)
    await message.delete()
    await bot.send_message(message.from_user.id, '{}'.format(type_search_text), reply_markup=menu_cancel)
    mess_id = await bot.send_message(message.from_user.id, '{}'.format(city_search_text))
    async with state.proxy() as data:
        data['mess_id'] = mess_id['message_id']


def register_handlers_highrice_start(dp: Dispatcher):
    dp.register_message_handler(highprice_start, text='💰 Дорогие отели', state=None)
    dp.register_message_handler(highprice_start, commands=['highprice', 'Дорогие отели'], state=None)
    dp.register_message_handler(cancel, text='❌ Отмена', state='*')
    dp.register_message_handler(get_city, state=UserInfoState.start)
    dp.register_callback_query_handler(set_city, Text(startswith='city_'), state=UserInfoState.city)
    dp.register_callback_query_handler(start_year, Text(startswith='startyear'), state=UserInfoState.start_year)
    dp.register_callback_query_handler(start_month, Text(startswith='startmonth'), state=UserInfoState.start_month)
    dp.register_callback_query_handler(start_day, Text(startswith='startday'), state=UserInfoState.start_day)
    dp.register_callback_query_handler(start_check, Text(startswith='startcheck'), state=UserInfoState.start_check)
    dp.register_callback_query_handler(end_year, Text(startswith='endyear'), state=UserInfoState.end_year)
    dp.register_callback_query_handler(end_month, Text(startswith='endmonth'), state=UserInfoState.end_month)
    dp.register_callback_query_handler(end_day, Text(startswith='endday'), state=UserInfoState.end_day)
    dp.register_callback_query_handler(end_check, Text(startswith='endcheck'), state=UserInfoState.end_check)
    dp.register_callback_query_handler(adult_num, Text(startswith='adult'), state=UserInfoState.adults)
    dp.register_callback_query_handler(child_worker, Text(startswith='childnum'), state=UserInfoState.child)
    dp.register_callback_query_handler(age_worker, Text(startswith='child_age'), state=UserInfoState.agechilds)
    dp.register_callback_query_handler(check, Text(startswith='final'), state=UserInfoState.check)
    dp.register_callback_query_handler(hotels, Text(startswith='hotel'), state=UserInfoState.search)
    dp.register_callback_query_handler(hotel_foto, Text(startswith='photo'), state=UserInfoState.search)
