from loader import bot
from aiogram import types
from aiogram.dispatcher import Dispatcher, FSMContext
from states.user_state import UserInfoState
from keyboards.reply.menu import menu_cancel
from handlers.common.cancel import cancel
from database.database import get_search_info, search_id_one, get_hotel_list, get_photo_list, get_hotel_one
from keyboards.inline.history_key import get_history_key, get_hotel_history_key, get_one_hotel_key
from aiogram.dispatcher.filters import Text
from utils.history_utils import history_book_description, history_hotel_description
from keyboards.inline.photo_key import get_photo_keyboard


# @dp.message_handler(text='📙 История поиска', state=None)
# @dp.message_handler(commands=['history', 'История поиска'], state=None)
async def history_start(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    search_info = await get_search_info(user_id)
    async with state.proxy() as data:
        data['search_info'] = search_info
    if not search_info:
        await bot.send_message(message.from_user.id, 'История поиска отсутствует. Поиск отелей еще не выполнялся! Нажмите - Отмена.', reply_markup=menu_cancel)
    else:
        await UserInfoState.start.set()
        history_key = await get_history_key(search_info)
        await bot.send_message(message.from_user.id, 'История поиска!', reply_markup=menu_cancel)
        await bot.send_message(message.from_user.id, 'Выберите ниже необходимый поиск, для детализации: ',
                               reply_markup=history_key)


# @dp.callback_query_handler(Text(startswith='searchid'), state=UserInfoState.start)
async def search_info(callback_query: types.CallbackQuery, state: FSMContext):
    search_id = callback_query.data.split('_')[1]
    data_search = await search_id_one(search_id)
    descr_text = await history_book_description(data_search)
    search_id = data_search[0][2]
    hotel_list = await get_hotel_list(search_id)
    async with state.proxy() as data:
        data['hotel_list'] = hotel_list
    hotels_key = await get_hotel_history_key(hotel_list)
    await UserInfoState.hotelinfo.set()
    mess_id = await bot.send_message(callback_query.from_user.id, text=descr_text, reply_markup=hotels_key, parse_mode="html")
    async with state.proxy() as data:
        data['hotel_mess_id'] = mess_id['message_id']


# @dp.callback_query_handler(Text(startswith='hot_'), state=UserInfoState.hotelinfo)
async def hotel_info(callback_query: types.CallbackQuery, state: FSMContext):
    search_id = callback_query.data.split('_')[1]
    hotel_id = callback_query.data.split('_')[2]
    if search_id == '555' and hotel_id == '555':
        async with state.proxy() as data:
            mess_id = data['hotel_mess_id']
        await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id)
        await UserInfoState.start.set()
    else:
        hotel_one = await get_hotel_one(search_id, hotel_id)
        hotel_text = await history_hotel_description(hotel_one)
        # photo_list = await get_photo_list(search_id, hotel_id)
        # print(photo_list)
        hotel_key = await get_one_hotel_key(search_id, hotel_id)
        await UserInfoState.photoinfo.set()
        mess_id = await bot.send_message(callback_query.from_user.id, text=hotel_text, reply_markup=hotel_key,
                                        parse_mode="html")
        async with state.proxy() as data:
            data['one_hotel_mess_id'] = mess_id['message_id']


# @dp.callback_query_handler(Text(startswith='one_'), state=UserInfoState.photoinfo)
async def photo_info(callback_query: types.CallbackQuery, state: FSMContext):
    search_id = callback_query.data.split('_')[1]
    hotel_id = callback_query.data.split('_')[2]
    if search_id == '555' and hotel_id == '555':
        async with state.proxy() as data:
            mess_id = data['one_hotel_mess_id']
        await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id)
        await UserInfoState.hotelinfo.set()
    else:
        photo_list = await get_photo_list(search_id, hotel_id)
        async with state.proxy() as data:
            data['photo_list'] = photo_list
        number = len(photo_list)
        if number == 0:
            photo_keyboard = await get_photo_keyboard(0, number)
            mess_id = await bot.send_message(callback_query.from_user.id, text='Просмотренные фотографии отсутствуют!', reply_markup=photo_keyboard)
            async with state.proxy() as data:
                data['photo_mess_id'] = mess_id['message_id']
        elif number == 1:
            photo_keyboard = await get_photo_keyboard(0, 0)
            mess_id = await bot.send_photo(callback_query.from_user.id, photo=photo_list[0][3], reply_markup=photo_keyboard)
            async with state.proxy() as data:
                data['photo_mess_id'] = mess_id['message_id']
        else:
            photo_keyboard = await get_photo_keyboard(0, number)
            mess_id = await bot.send_photo(callback_query.from_user.id, photo=photo_list[0][3], reply_markup=photo_keyboard)
            async with state.proxy() as data:
                data['photo_mess_id'] = mess_id['message_id']
                data['index_photo'] = 0


# @dp.callback_query_handler(Text(startswith='photo_'), state=UserInfoState.photoinfo)
async def photo_show(callback_query: types.CallbackQuery, state: FSMContext):
    result = callback_query.data.split('_')[1]
    if result == 'next':
        async with state.proxy() as data:
            index = data['index_photo']
            photo_list = data['photo_list']
            mess_id = data['photo_mess_id']
        index += 1
        photo_keyboard = await get_photo_keyboard(index, len(photo_list))
        async with state.proxy() as data:
            data['index_photo'] = index
        await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id)
        mess_id = await bot.send_photo(callback_query.from_user.id, photo=photo_list[index][3], reply_markup=photo_keyboard)
        async with state.proxy() as data:
            data['photo_mess_id'] = mess_id['message_id']
    elif result == 'back':
        async with state.proxy() as data:
            index = data['index_photo']
            photo_list = data['photo_list']
            mess_id = data['photo_mess_id']
        index -= 1
        photo_keyboard = await get_photo_keyboard(index, len(photo_list))
        await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id)
        mess_id = await bot.send_photo(callback_query.from_user.id, photo=photo_list[index][3], reply_markup=photo_keyboard)
        async with state.proxy() as data:
            data['photo_mess_id'] = mess_id['message_id']
            data['index_photo'] = index
    elif result == 'cancel':
        async with state.proxy() as data:
            mess_id = data['photo_mess_id']
        await bot.delete_message(chat_id=callback_query.message.chat.id, message_id=mess_id)


def register_handlers_history_start(dp: Dispatcher):
    dp.register_message_handler(history_start, text='📙 История поиска', state=None)
    dp.register_message_handler(history_start, commands=['history', 'История поиска'], state=None)
    dp.register_message_handler(cancel, text='❌ Отмена', state='*')
    dp.register_callback_query_handler(search_info, Text(startswith='searchid'), state=UserInfoState.start)
    dp.register_callback_query_handler(hotel_info, Text(startswith='hot_'), state=UserInfoState.hotelinfo)
    dp.register_callback_query_handler(photo_info, Text(startswith='one_'), state=UserInfoState.photoinfo)
    dp.register_callback_query_handler(photo_show, Text(startswith='photo_'), state=UserInfoState.photoinfo)
