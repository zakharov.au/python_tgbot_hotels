from loader import bot
from aiogram import types
from aiogram.dispatcher import Dispatcher, FSMContext
from states.user_state import UserInfoState
from keyboards.reply.menu import menu_cancel
from keyboards.inline.date_key import years_keyboard
from aiogram.dispatcher.filters import Text
from utils.get_command_text import get_city_search_text, get_type_search_text
from handlers.common.get_date import start_year, start_month, start_day, start_check, end_year, end_month, end_day
from handlers.common.cancel import cancel
from handlers.common.people import end_check, adult_num, child_worker, age_worker
from handlers.common.hotels import hotels, hotel_foto
from handlers.common.city import get_city, set_city
from handlers.common.final_check import check
from database.database import get_new_searchid


# @dp.message_handler(text='🏛 Наиболее подходящие по цене и расположению отели', state=None)
# @dp.message_handler(commands=['bestdeal', 'Наиболее подходящие по цене и расположению отели'], state=None)
async def bestdeal_start(message: types.Message, state: FSMContext):
    command = 'bestdeal'
    user_id = message.from_user.id
    search_id, time = await get_new_searchid()
    async with state.proxy() as data:
        data['command'] = command
        data['user_id'] = user_id
        data['time'] = time
        data['search_id'] = search_id
    await UserInfoState.start.set()
    type_search_text = await get_type_search_text(command)
    city_search_text = await get_city_search_text(command)
    await message.delete()
    await bot.send_message(message.from_user.id, '{}'.format(type_search_text), reply_markup=menu_cancel)
    mess_id = await bot.send_message(message.from_user.id, '{}'.format(city_search_text))
    async with state.proxy() as data:
        data['mess_id'] = mess_id['message_id']


# @dp.message_handler(state=UserInfoState.distance)
async def get_distance(message: types.Message, state: FSMContext):
    distance = message.text
    async with state.proxy() as data:
        data['distance'] = distance
        mess_id = data['mess_id']
    await bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    await bot.delete_message(chat_id=message.chat.id, message_id=mess_id)
    await bot.send_message(message.from_user.id, text='🚶 Выбрано максимально допустимое расстояние от отеля до центра '
                                                      'города - не более {} км.'.format(distance))
    await UserInfoState.priceMin.set()
    mess_id = await bot.send_message(message.from_user.id, text='💵 Укажите минимально допустимую стоимость проживания '
                                                                'за сутки в долларах США:')
    async with state.proxy() as data:
        data['mess_id'] = mess_id['message_id']


# @dp.message_handler(state=UserInfoState.priceMin)
async def get_price_min(message: types.Message, state: FSMContext):
    priceMin = message.text
    async with state.proxy() as data:
        data['priceMin'] = priceMin
        mess_id = data['mess_id']
    await bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    await UserInfoState.priceMax.set()
    await bot.delete_message(chat_id=message.chat.id, message_id=mess_id)
    await bot.send_message(message.from_user.id,
                           text='💵 Выбрана минимально допустимая стоимость проживания за сутки - не менее {}$'.format(
                               priceMin))
    mess_id = await bot.send_message(message.from_user.id, text='💵 Укажите максимально допустимую стоимость проживания '
                                                                'за сутки в долларах США:')
    async with state.proxy() as data:
        data['mess_id'] = mess_id['message_id']


# @dp.message_handler(state=UserInfoState.priceMax)
async def get_price_max(message: types.Message, state: FSMContext):
    priceMax = message.text
    async with state.proxy() as data:
        data['priceMax'] = priceMax
        mess_id = data['mess_id']
    year_keyboard = await years_keyboard('start')
    await UserInfoState.start_year.set()
    await bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    await bot.delete_message(chat_id=message.chat.id, message_id=mess_id)
    await bot.send_message(message.from_user.id, text='💵 Выбрана максимально допустимая стоимость проживания за сутки - '
                                                      'не более {}$'.format(priceMax))
    await bot.send_message(message.from_user.id, text='📆 Выберите год начала аренды:',
                           reply_markup=year_keyboard)


def register_handlers_bestdeal_start(dp: Dispatcher):
    dp.register_message_handler(bestdeal_start, text='🏛 Наиболее подходящие по цене и расположению отели', state=None)
    dp.register_message_handler(bestdeal_start, commands=['bestdeal', 'Наиболее подходящие по цене и расположению '
                                                                      'отели'], state=None)
    dp.register_message_handler(cancel, text='❌ Отмена', state='*')
    dp.register_message_handler(get_city, state=UserInfoState.start)
    dp.register_callback_query_handler(set_city, Text(startswith='city_'), state=UserInfoState.city)
    dp.register_message_handler(get_distance, state=UserInfoState.distance)
    dp.register_message_handler(get_price_min, state=UserInfoState.priceMin)
    dp.register_message_handler(get_price_max, state=UserInfoState.priceMax)
    dp.register_callback_query_handler(start_year, Text(startswith='startyear'), state=UserInfoState.start_year)
    dp.register_callback_query_handler(start_month, Text(startswith='startmonth'), state=UserInfoState.start_month)
    dp.register_callback_query_handler(start_day, Text(startswith='startday'), state=UserInfoState.start_day)
    dp.register_callback_query_handler(start_check, Text(startswith='startcheck'), state=UserInfoState.start_check)
    dp.register_callback_query_handler(end_year, Text(startswith='endyear'), state=UserInfoState.end_year)
    dp.register_callback_query_handler(end_month, Text(startswith='endmonth'), state=UserInfoState.end_month)
    dp.register_callback_query_handler(end_day, Text(startswith='endday'), state=UserInfoState.end_day)
    dp.register_callback_query_handler(end_check, Text(startswith='endcheck'), state=UserInfoState.end_check)
    dp.register_callback_query_handler(adult_num, Text(startswith='adult'), state=UserInfoState.adults)
    dp.register_callback_query_handler(child_worker, Text(startswith='childnum'), state=UserInfoState.child)
    dp.register_callback_query_handler(age_worker, Text(startswith='child_age'), state=UserInfoState.agechilds)
    dp.register_callback_query_handler(check, Text(startswith='final'), state=UserInfoState.check)
    dp.register_callback_query_handler(hotels, Text(startswith='hotel'), state=UserInfoState.search)
    dp.register_callback_query_handler(hotel_foto, Text(startswith='photo'), state=UserInfoState.search)
