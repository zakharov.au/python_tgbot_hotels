from loader import bot
from aiogram import types
from aiogram.dispatcher import Dispatcher, FSMContext
from keyboards.reply.menu import menu_cancel


# @dp.message_handler(text='📖 Справка', state=None)
# @dp.message_handler(commands=['help', 'Справка'], state=None)
async def help_start(message: types.Message, state: FSMContext):
    text = 'Команды:\n<b>/start</b> - Запускает работу бота\n<b>/help</b> - Вывод справки по командам ' \
           'бота\n<b>/lowprice</b> - Поиск ' \
           'дешевых отелей по возрастанию цены\n<b>/highprice</b> -Поиск дорогих отелей по убыванию ' \
           'цены\n<b>/bestdeal</b> - Поиск ' \
           'отелей наиболее подходящих по цене и расположению: потребуется ввести диапазон цен и максимальную ' \
           'удаленность от центра\n<b>/history</b> - История поиска. Просмотренные отели и фотографии. '
    await bot.send_message(message.from_user.id, text=text, reply_markup=menu_cancel, parse_mode="html")


def register_handlers_help_start(dp: Dispatcher):
    dp.register_message_handler(help_start, text='📖 Справка', state=None)
