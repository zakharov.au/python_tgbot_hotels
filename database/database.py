import sqlite3
from datetime import datetime
from utils.get_hotels import change_format


def db_start():
    global base, cur
    base = sqlite3.connect('bot.db')
    cur = base.cursor()
    if base:
        print('Database started')
    base.execute('CREATE TABLE IF NOT EXISTS searchid(num, id)')
    base.execute('CREATE TABLE IF NOT EXISTS history(userid, time, searchid PRIMARY KEY, typesearch, city, '
                 'start_price, end_price, distance, start_date, end_date, person, childs, child_age)')
    base.execute('CREATE TABLE IF NOT EXISTS hotels(userid, searchid, hotelid, hotel_name, landmarks, address, '
                 'userRating, starRating, price, total_price)')
    base.execute('CREATE TABLE IF NOT EXISTS photos(userid, searchid, hotelid, photolink)')
    base.commit()


async def get_new_searchid():
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    searchid = cur.execute('SELECT * FROM searchid').fetchall()
    if searchid == []:
        new_id = str(10000)
        cur.execute('INSERT INTO searchid VALUES(?, ?)', ('idid', '{}'.format(new_id)))
    else:
        new_id = str(int(searchid[0][1]) + 1)
        cur.execute('UPDATE searchid SET id == ? WHERE num = ?', ('{}'.format(new_id), 'idid'))
    base.commit()
    return new_id, time


async def add_db_data(data):
    global cur
    userid = data['user_id']
    time = data['time']
    search_id = data['search_id']
    city = data['city']
    checkin = '{}-{}-{}'.format(data['start_year'], await change_format(data['start_month']),
                                await change_format(data['start_day']))
    checkout = '{}-{}-{}'.format(data['end_year'], await change_format(data['end_month']),
                                 await change_format(data['end_day']))
    adults = data['adults']
    childlist = data.get('agechilds', 0)

    command = data['command']
    if command == 'lowprice' or command == 'highprice':
        price_start = '555555'
        price_end = '555555'
        distance = '555555'
    elif command == 'bestdeal':
        price_start = data['priceMin']
        price_end = data['priceMax']
        distance = data['distance']
    if childlist != 0:
        childs = str(len(childlist))
        child_age = ''
        for i_child in range(len(childlist)):
            if i_child + 1 < len(childlist):
                child_age += childlist[i_child]
                child_age += '_'
            else:
                child_age += childlist[i_child]
    else:
        childs = '0'
        child_age = ''
    cur.execute('''INSERT INTO history 
    (userid, time, searchid, typesearch, city, start_price, end_price, distance, start_date, end_date, person, childs, child_age) 
    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
    (userid, time, search_id, command, city, price_start, price_end, distance, checkin, checkout, adults, childs, child_age))
    base.commit()


async def hotels_db_add(info: list, index: int):
    global cur
    starrating = str(info[index]['starRating'])
    dist = str(info[index]['landmarks'])
    hotel_name = str(info[index]['hotel_name'])
    address = str(info[index]['address'])
    userrating = str(info[index]['userRating'])
    price = str(info[index]['price'])
    total_price = str(info[index]['total_price'])
    user_id = str(info[index]['user_id'])
    search_id = str(info[index]['search_id'])
    hotel_id = str(info[index]['hotel_id'])
    cur.execute('''INSERT INTO hotels (userid, searchid, hotelid, hotel_name, landmarks, address, userRating, 
    starRating, price, total_price) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', (user_id, search_id, hotel_id,
                                                                              hotel_name, dist, address, userrating,
                                                                              starrating, price, total_price))
    base.commit()


async def photo_db_add(user_id, search_id, hotelid, photo_link):
    cur.execute('''INSERT INTO photos (userid, searchid, hotelid, photolink) VALUES(?, ?, ?, ?)''',
                (user_id, str(search_id), str(hotelid), photo_link))
    base.commit()


async def get_search_info(user_id):
    data_search = cur.execute('''SELECT * FROM history WHERE userid = ?''', (user_id, )).fetchall()
    return data_search


async def search_id_one(search_id):
    data_search = cur.execute('''SELECT * FROM history WHERE searchid = ?''', (search_id,)).fetchall()
    return data_search


async def get_hotel_list(search_id):
    search_list = cur.execute('''SELECT * FROM hotels WHERE searchid = ?''', (search_id,)).fetchall()
    return search_list


async def get_hotel_one(search_id, hotel_id):
    search_list = cur.execute('''SELECT * FROM hotels WHERE searchid = ? AND hotelid = ?''', (search_id, hotel_id)).fetchall()
    return search_list


async def get_photo_list(search_id, hotel_id):
    search_list = cur.execute('''SELECT * FROM photos WHERE searchid = ? AND hotelid = ?''', (search_id, hotel_id)).fetchall()
    return search_list
