from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

key1 = KeyboardButton('📖 Справка')
key2 = KeyboardButton('💵 Бюджетные отели')
key3 = KeyboardButton('💰 Дорогие отели')
key4 = KeyboardButton('📙 История поиска')
key5 = KeyboardButton('🏛 Наиболее подходящие по цене и расположению отели')
menu_keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
menu_keyboard.add(key2, key3)
menu_keyboard.add(key5)
menu_keyboard.add(key1, key4)


menu_cancel = ReplyKeyboardMarkup(resize_keyboard=True)
menu_cancel.add(KeyboardButton('❌ Отмена'))
