from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def yes_no_keyboard_final():
    yes_no_keyboard_final = InlineKeyboardMarkup(row_width=2)
    key_yes_end = InlineKeyboardButton(text='✅ Да', callback_data='final_yes')
    key_no_end = InlineKeyboardButton(text='❌ Нет', callback_data='final_no')
    yes_no_keyboard_final.add(key_yes_end, key_no_end)
    return yes_no_keyboard_final
