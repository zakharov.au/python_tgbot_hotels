from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def get_city_keyboard(city_dict):
    city_keyboard = InlineKeyboardMarkup(row_width=1)

    for i_city in city_dict.keys():
        key = InlineKeyboardButton(text='{}. {}.'.format(i_city, city_dict[i_city][0]), callback_data='city_{}'.format(i_city))
        city_keyboard.add(key)
    return city_keyboard


