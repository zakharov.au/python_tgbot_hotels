from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import calendar
from datetime import date


async def years_keyboard(status: str):
    years_keyboard = InlineKeyboardMarkup(row_width=6)
    current_year = int(date.today().year)
    keys = list()
    for i_year in range(6):
        year = current_year + i_year
        key = InlineKeyboardButton(text=str(year), callback_data='{}year_{}'.format(status, str(year)))
        keys.append(key)
    years_keyboard.row(*keys)
    return years_keyboard


async def month_keyboard(status: str, start_year):
    months_keyboard = InlineKeyboardMarkup(row_width=4)
    current_month = date.today().month
    current_year = date.today().year
    months = {1: 'Январь', 2: 'Февраль', 3: 'Март', 4: 'Апрель', 5: 'Май',
              6: 'Июнь', 7: 'Июль', 8: 'Август', 9: 'Сентябрь', 10: 'Октябрь',
              11: 'Ноябрь', 12: 'Декабрь'}
    month_key = list()
    for i_month in range(1, 13):
        if i_month < int(current_month) and int(current_year) == int(start_year):
            key = InlineKeyboardButton(text='▫️', callback_data='___', time=5)
        else:
            key = InlineKeyboardButton(text=months[i_month], callback_data='{}month_{}'.format(status, str(i_month)))
        month_key.append(key)
    months_keyboard.add(*month_key)
    change_year_key = InlineKeyboardButton(text='Выбранный год - {}. Нажмите чтобы поменять.'.format(start_year), callback_data='{}month_year_change'.format(status))
    months_keyboard.add(change_year_key)
    return months_keyboard


async def days_keyboard(status: str, year, month):
    months = {1: 'Январь', 2: 'Февраль', 3: 'Март', 4: 'Апрель', 5: 'Май',
              6: 'Июнь', 7: 'Июль', 8: 'Август', 9: 'Сентябрь', 10: 'Октябрь',
              11: 'Ноябрь', 12: 'Декабрь'}
    days_keyboard = InlineKeyboardMarkup(row_width=7)
    key_1 = InlineKeyboardButton(text='ПН', callback_data='--')
    key_2 = InlineKeyboardButton(text='ВТ', callback_data='--')
    key_3 = InlineKeyboardButton(text='СР', callback_data='--')
    key_4 = InlineKeyboardButton(text='ЧТ', callback_data='--')
    key_5 = InlineKeyboardButton(text='ПТ', callback_data='--')
    key_6 = InlineKeyboardButton(text='СБ', callback_data='--')
    key_7 = InlineKeyboardButton(text='ВС', callback_data='--')
    days_keyboard.add(key_1, key_2, key_3, key_4, key_5, key_6, key_7)
    current_month = int(date.today().month)
    current_year = int(date.today().year)
    current_day = int(date.today().day)
    last_day = 1
    first_day = calendar.monthrange(int(year), int(month))[0]
    number_days = calendar.monthrange(int(year), int(month))[1]
    first_keys = []
    for i_day in range(7):
        if i_day < first_day:
            key = InlineKeyboardButton(text=' ', callback_data='---')
        elif last_day < current_day and int(year) == current_year and int(month) == current_month:
            key = InlineKeyboardButton(text='▫️', callback_data='---')
            last_day += 1
        elif last_day == current_day and int(year) == current_year and int(month) == current_month:
            key_text = str(last_day)
            key = InlineKeyboardButton(text='*{}*'.format(key_text), callback_data='{}day_{}'.format(status, key_text))
            last_day += 1
        else:
            key_text = str(last_day)
            key = InlineKeyboardButton(text=key_text, callback_data='{}day_{}'.format(status, last_day))
            last_day += 1
        first_keys.append(key)
    days_keyboard.add(*first_keys)
    while last_day <= number_days:
        new_keys = []
        for i_day in range(last_day, last_day + 7):
            if i_day > number_days:
                key = InlineKeyboardButton(text=' ', callback_data='---')
            elif last_day < current_day and int(year) == current_year and int(month) == current_month:
                key = InlineKeyboardButton(text='▫️', callback_data='---')
                last_day += 1
            elif last_day == current_day and int(year) == current_year and int(month) == current_month:
                key_text = str(last_day)
                key = InlineKeyboardButton(text='*{}*'.format(key_text), callback_data='{}day_{}'.format(status, key_text))
                last_day += 1
            else:
                key_text = str(last_day)
                key = InlineKeyboardButton(text=key_text, callback_data='{}day_{}'.format(status, key_text))
                last_day += 1
            new_keys.append(key)
        days_keyboard.add(*new_keys)
    key_month = InlineKeyboardButton(text='Поменять месяц - {}'.format(months[int(month)]), callback_data='{}day_month'.format(status))
    key_year = InlineKeyboardButton(text='Поменять год - {}'.format(year), callback_data='{}day_year'.format(status))
    days_keyboard.add(key_year, key_month)
    return days_keyboard


async def yes_no_keyboard_date(status):
    yes_no_keyboard = InlineKeyboardMarkup(row_width=2)
    key_yes = InlineKeyboardButton(text='✅ Да', callback_data='{}check_yes'.format(status))
    key_no = InlineKeyboardButton(text='❌ Нет', callback_data='{}check_no'.format(status))
    yes_no_keyboard.add(key_yes, key_no)
    return yes_no_keyboard
