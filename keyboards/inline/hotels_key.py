from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def hotel_keyboard(hotel_list: list, index: int, page: int):
    hotel_keyboard = InlineKeyboardMarkup(row_width=2)
    back = InlineKeyboardButton(text='⬅ предыдущий️', callback_data='hotel_back_{}'.format(str(index)))
    next = InlineKeyboardButton(text='следующий ➡️', callback_data='hotel_next_{}'.format(str(index)))
    location = InlineKeyboardButton(text='🌍 На карте', url='https://maps.google.com/?daddr={},{}'.format(
        hotel_list[index]['coordinate']['lat'],
        hotel_list[index]['coordinate']['lon']))
    foto = InlineKeyboardButton(text='📷 Фото', callback_data='hotel_foto_{}'.format(hotel_list[index]['hotel_id']))
    booking = InlineKeyboardButton(text='🧰 Забронировать', url=hotel_list[index]['link'])
    if index == 0 and page <= 1:
        hotel_keyboard.add(next)
    elif index > 0 or page > 1:
        hotel_keyboard.add(back, next)
    hotel_keyboard.add(location)
    hotel_keyboard.add(foto)
    hotel_keyboard.add(booking)
    return hotel_keyboard

    #https://maps.google.com/?daddr=43.12345,-76.12345