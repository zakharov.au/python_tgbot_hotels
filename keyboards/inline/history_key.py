from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def get_history_key(search_info: list):
    search_keyboard = InlineKeyboardMarkup(row_width=1)
    for i_search in search_info:
        datetime = i_search[1].split()
        date = datetime[0]
        time = datetime[1]
        key = InlineKeyboardButton(text='Дата - {}. Время - {}. {} - {}'.format(date, time, i_search[4], i_search[3]),
                                   callback_data='searchid_{}'.format(i_search[2]))
        search_keyboard.add(key)
    return search_keyboard


async def get_hotel_history_key(hotel_list):
    hotels_keyboard = InlineKeyboardMarkup(row_width=1)
    if not hotel_list:
        key = InlineKeyboardButton(text='Ничего не найдено!', callback_data='hit_0')
        hotels_keyboard.add(key)
    else:
        for i_hotel in hotel_list:
            hotel_name = i_hotel[3]
            search_id = i_hotel[1]
            hotel_id = i_hotel[2]
            key = InlineKeyboardButton(text='Отель - {}'.format(hotel_name),
                                       callback_data='hot_{}_{}'.format(search_id, hotel_id))
            hotels_keyboard.add(key)
    key_cancel = InlineKeyboardButton(text='❌ Закрыть', callback_data='hot_555_555')
    hotels_keyboard.add(key_cancel)
    return hotels_keyboard


async def get_one_hotel_key(search_id, hotel_id):
    hotels_keyboard = InlineKeyboardMarkup(row_width=1)
    key_photo = InlineKeyboardButton(text='📷 Просмотренные фотографии', callback_data='one_{}_{}'.format(search_id, hotel_id))
    key_cancel = InlineKeyboardButton(text='❌ Закрыть', callback_data='one_555_555')
    hotels_keyboard.add(key_photo)
    hotels_keyboard.add(key_cancel)
    return hotels_keyboard
