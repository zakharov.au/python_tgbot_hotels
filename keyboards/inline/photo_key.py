from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def get_photo_keyboard(index: int, number: int):
    city_keyboard = InlineKeyboardMarkup(row_width=2)
    key_next = InlineKeyboardButton(text='вперед ▶️', callback_data='photo_next')
    key_back = InlineKeyboardButton(text='◀ назад', callback_data='photo_back')
    key_cancel = InlineKeyboardButton(text='❌ закрыть фотографии', callback_data='photo_cancel')
    if number == 0:
        pass
    elif index == 0:
        city_keyboard.add(key_next)
    elif 0 < index < number - 1:
        city_keyboard.add(key_back, key_next)
    elif index == number - 1:
        city_keyboard.add(key_back)
    city_keyboard.add(key_cancel)
    return city_keyboard
