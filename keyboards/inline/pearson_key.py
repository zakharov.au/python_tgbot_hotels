from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


async def adult_num_keyboard():
    adults_keyboard = InlineKeyboardMarkup(row_width=8)
    key_list_adult = list()
    for i_key in range(1, 9):
        key = InlineKeyboardButton(text=str(i_key), callback_data='adult_{}'.format(str(i_key)))
        key_list_adult.append(key)
    adults_keyboard.add(*key_list_adult)
    return adults_keyboard


async def child_num_keyboard():
    child_keyboard = InlineKeyboardMarkup(row_width=5)
    key_list_child = list()
    for i_key in range(0, 10):
        key = InlineKeyboardButton(text=str(i_key), callback_data='childnum_{}'.format(str(i_key)))
        key_list_child.append(key)
    child_keyboard.add(*key_list_child)
    return child_keyboard


async def age_child_keyboard(num_child: int):
    age_keyboard = InlineKeyboardMarkup(row_width=8)
    key_age_list_child = list()
    for i_age in range(1, 17):
        key_age = InlineKeyboardButton(text=str(i_age), callback_data='child_age_{}_{}'.format(num_child, i_age))
        key_age_list_child.append(key_age)
    age_keyboard.add(*key_age_list_child)
    return age_keyboard

