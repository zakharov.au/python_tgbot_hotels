async def history_book_description(data):
    datetime = data[0][1].split()
    date = datetime[0]
    time = datetime[1]
    city = data[0][4]
    start_date = data[0][8]
    end_date = data[0][9]
    adults = data[0][10]
    childs = data[0][11]
    age_childs = data[0][12]
    command = data[0][3]
    age_text = ''
    if int(childs) > 0:
        ages = age_childs.split('_')
        for i_child in ages:
            age_text = age_text + str(i_child) + ' лет '
        text = '📆 Дата поиска - {} \n🕰 Время поиска - {} \n🔍 Тип поиска - \{} \n🏙 Город поиска- {} \n📅 Дата ' \
               'начала аренды - {} \n📅 Дата завершения аренды - {} \n👨 Взрослых - {} чел. \n 👧‍👦 Детей - {} чел. ' \
               '({}). '.format(
            date,
            time,
            command,
            city,
            start_date,
            end_date,
            adults,
            childs,
            age_text)
    else:
        text = '📆 Дата поиска - {} \n🕰 Время поиска - {} \n🔍 Тип поиска - \{} \n🏙 Город поиска- {} \n📅 Дата ' \
               'начала аренды - {} \n📅 Дата завершения аренды - {} \n👨 Взрослых - {} чел.  '.format(
            date,
            time,
            command,
            city,
            start_date,
            end_date,
            adults
        )
    if command == 'bestdeal':
        pricemin = data[0][5]
        pricemax = data[0][6]
        distance = data[0][7]
        text_end = '\n 🚶 Расстояние до центра - не более {} км.\n 💵Стоимость - от {} до {} $ за ' \
                   'сутки\n<b>Просмотренные отели</b>(нажмите для просмотра информации): '.format(
            distance,
            pricemin,
            pricemax
        )
    else:
        text_end = '\n<b>Просмотренные отели</b>(нажмите для просмотра информации): '
    descr_text = text + text_end
    return descr_text


async def history_hotel_description(hotel_one):
    hotel_name = hotel_one[0][3]
    landmarks = hotel_one[0][4]
    address = hotel_one[0][5]
    userRating = hotel_one[0][6]
    starRating = hotel_one[0][7]
    price = hotel_one[0][8]
    total_price = hotel_one[0][9]
    if landmarks == '5000':
        dist_text = 'Нет информации'
    else:
        dist_text = '{} км.'.format(str(landmarks))
    stars = await get_star(starRating)
    descr_txt = '<b>{}</b> {} \n📫 Адрес: {}\n🥇 Рейтинг: {}\n🚶 Расстояние до центра: {}\n💵 Стоимость за сутки - ' \
                '{} $\n💰 Общая стоимость - {} $: '.format(
        hotel_name,
        stars,
        address,
        userRating,
        dist_text,
        price,
        total_price
    )
    return descr_txt


async def get_star(starRating):
    try:
        stars = ''
        for i_star in range(int(starRating[:1])):
            stars += '⭐️'
        return stars
    except:
        stars = ''
        return stars
