from database.database import hotels_db_add


async def get_description(info: list, index: int):
    await hotels_db_add(info, index)
    try:
        stars = ''
        for i_star in range(int(info[index]['starRating'])):
            stars += '⭐️'
        description = await get_text_description(info, index, stars)
        return description
    except:
        stars = ''
        description = await get_text_description(info, index, stars)
        return description


async def get_text_description(info: list, index: int, stars):
    if info[index]['command'] == 'bestdeal':
        if info[index]['landmarks'] == 5000:
            dist_text = 'Нет информации'
        else:
            dist_text = '{} км.'.format(str(info[index]['landmarks']))
        descr_txt = '<b>{}</b> {} \n📫 Адрес: {}\n🥇 Рейтинг: {}\n🚶 Расстояние до центра: {}\n💵 Стоимость за сутки - ' \
                    '{} $(без учета налогов)\n💰 Общая стоимость - {} $(без учета налогов)'.format(
            info[index]['hotel_name'],
            stars,
            info[index]['address'],
            info[index]['userRating'],
            dist_text,
            info[index]['price'],
            info[index]['total_price']
        )
    else:
        if info[index]['landmarks'] == 5000:
            dist_text = 'Нет информации'
        else:
            dist_text = '{} км.'.format(str(info[index]['landmarks']))
        descr_txt = '<b>{}</b> {} \n📫 Адрес: {}\n🥇 Рейтинг: {}\n🚶 Расстояние до центра: {}\n💵 Стоимость за сутки - ' \
                    '{} $\n💰 Общая стоимость - {} $'.format(
            info[index]['hotel_name'],
            stars,
            info[index]['address'],
            info[index]['userRating'],
            dist_text,
            info[index]['price'],
            info[index]['total_price']
        )

    return descr_txt
