from config_data.config import RAPID_API_KEY
import aiohttp


async def get_hotel_photo(hotel_id: str):
    url = "https://hotels4.p.rapidapi.com/properties/get-hotel-photos"
    querystring = {"id": hotel_id}
    headers = {
        "X-RapidAPI-Key": RAPID_API_KEY,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"}
    # response = requests.request("GET", url, headers=headers, params=querystring)
    # data = json.loads(response.text)
    async with aiohttp.ClientSession() as session:
        async with session.get(url=url, headers=headers, params=querystring) as response:
            data: object = await response.json()
    photos_list = data['hotelImages']
    photo_link_list = list()
    for i_photo in photos_list:
        i_photo_link = i_photo['baseUrl'].replace('{size}', 'l')
        photo_link_list.append(i_photo_link)
    return photo_link_list


#print(get_hotel_photo('280086'))
