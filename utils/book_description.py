async def get_book_description(data):
    city = data['city']
    start_day = data['start_day']
    start_year = data['start_year']
    start_month = data['start_month']
    end_year = data['end_year']
    end_month = data['end_month']
    end_day = data['end_day']
    adults = data['adults']
    childs = data.get('child')
    ages = data.get('agechilds')
    command = data['command']
    age_text = ''
    if int(childs) > 0:
        for i_child in ages:
            age_text = age_text + str(i_child) + ' лет '
        text = '🏙 Выбранный город - {} \n📅 Дата начала аренды - {}-{}-{} \n📅 Дата завершения аренды - {}-{}-{} \n👨 Взрослых - {} чел. \n 👧‍👦 Детей - {} чел. ({}). '.format(
            city,
            start_year,
            start_month,
            start_day,
            end_year,
            end_month,
            end_day,
            adults,
            childs,
            age_text)
    else:
        text = '🏙 Выбранный город - {} \n📅 Дата начала аренды - {}-{}-{} \n📅 Дата завершения аренды - {}-{}-{} \n👨 Взрослых - {} чел.  '.format(
            city,
            start_year,
            start_month,
            start_day,
            end_year,
            end_month,
            end_day,
            adults
            )
    if command == 'bestdeal':
        pricemin = data['priceMin']
        pricemax = data['priceMax']
        distance = data['distance']
        text_end = '\n 🚶 Расстояние до центра - не более {} км.\n 💵Стоимость - от {} до {} $ за сутки\nВерно? '.format(
            distance,
            pricemin,
            pricemax
        )
    else:
        text_end = '\nВерно? '
    final_text = text + text_end
    return final_text
