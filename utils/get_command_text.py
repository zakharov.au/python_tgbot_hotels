

async def get_type_search_text(command):
    if command == 'lowprice':
        type_search = 'Выбран поиск самых дешёвых отелей!'
    elif command == 'highprice':
        type_search = 'Выбран поиск самых дорогих отелей!'
    elif command == 'bestdeal':
        type_search = 'Выбран поиск наиболее подходящих по цене и расположению отелей!'
    return type_search


async def get_city_search_text(command):
    if command == 'lowprice':
        city_search = '🏙 В каком городе нужно найти топ самых дешёвых отелей?'
    elif command == 'highprice':
        city_search = '🏙 В каком городе нужно найти топ самых дорогих отелей?'
    elif command == 'bestdeal':
        city_search = '🏙 В каком городе нужно найти наиболее подходящие по цене и расположению отели?'
    return city_search
