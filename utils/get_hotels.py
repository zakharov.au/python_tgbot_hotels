import aiohttp
import re
from config_data.config import RAPID_API_KEY


async def change_format(date: str):
    if len(date) < 2:
        new_date = '0' + date
        return new_date
    else:
        return date


async def hotel_search(data, command, pagenumber=1):
    try:
        # data = {'city': 'Центр Рима', 'city_id': '10818945', 'start_year': '2022', 'start_month': '9', 'start_day':
        # '17', 'end_year': '2022', 'end_month': '9', 'end_day': '24', 'adults': '2', 'child': '2', 'num_age': 2,
        # 'agechilds': ['6', '12']}
        url = 'https://hotels4.p.rapidapi.com/properties/list'
        city_id = data['city_id']
        checkin = '{}-{}-{}'.format(data['start_year'], await change_format(data['start_month']),
                                    await change_format(data['start_day']))
        checkout = '{}-{}-{}'.format(data['end_year'], await change_format(data['end_month']),
                                     await change_format(data['end_day']))
        userid = data['user_id']
        search_id = data['search_id']
        adults = data['adults']
        childlist = data.get('agechilds', 0)
        if command == 'lowprice':
            sortorder = "PRICE"
        elif command == 'highprice':
            sortorder = 'PRICE_HIGHEST_FIRST'
        elif command == 'bestdeal':
            sortorder = 'DISTANCE_FROM_LANDMARK'
            pricemin = data['priceMin']
            pricemax = data['priceMax']
            distance = int(data['distance'])
        if childlist != 0:
            children = ''
            for i_child in range(len(childlist)):
                children += childlist[i_child]
                if i_child < len(childlist) - 1:
                    children += ','
            querystring = {"destinationId": city_id,
                           "pageNumber": str(pagenumber),
                           "pageSize": "25",
                           "checkIn": checkin,
                           "checkOut": checkout,
                           "adults1": adults,
                           "children1": children,
                           "sortOrder": sortorder,
                           "locale": "en_US",
                           "currency": "USD"}
        else:
            querystring = {"destinationId": city_id,
                           "pageNumber": str(pagenumber),
                           "pageSize": "25",
                           "checkIn": checkin,
                           "checkOut": checkout,
                           "adults1": adults,
                           "sortOrder": sortorder,
                           "locale": "en_US",
                           "currency": "USD"}
        if command == 'bestdeal':
            querystring['priceMin'] = pricemin
            querystring['priceMax'] = pricemax
        headers = {
            "X-RapidAPI-Key": RAPID_API_KEY,
            "X-RapidAPI-Host": "hotels4.p.rapidapi.com"}
        async with aiohttp.ClientSession() as session:
            async with session.get(url=url, headers=headers, params=querystring) as response:
                response_data: object = await response.json()
        # response = requests.request("GET", url, headers=headers, params=querystring)
        # response_data = json.loads(response.text)
        results = response_data['data']['body']['searchResults']['results']
        # print(results)
        hotel_list = list()
        for i_hotel in results:
            hotel_dict = dict()
            hotel_dict['command'] = command
            hotel_dict['user_id'] = userid
            hotel_dict['search_id'] = search_id
            hotel_dict['hotel_name'] = i_hotel['name']
            hotel_dict['hotel_id'] = i_hotel['id']
            hotel_dict['starRating'] = i_hotel['starRating']
            street_address = i_hotel['address'].get('streetAddress')
            if street_address is not None:
                hotel_dict['address'] = '{}, {}, {}'.format(
                    i_hotel['address']['countryName'],
                    i_hotel['address']['locality'],
                    i_hotel['address'].get('streetAddress'))
            else:
                hotel_dict['address'] = '{}, {}'.format(
                    i_hotel['address']['countryName'],
                    i_hotel['address']['locality'])
            guest_reviews = i_hotel.get('guestReviews')
            if guest_reviews is not None:
                hotel_dict['userRating'] = guest_reviews.get('rating')
            else:
                hotel_dict['userRating'] = 'Рейтинг отсутствует'
            total_price_str = i_hotel['ratePlan']['price']['fullyBundledPricePerStay']
            nights = int(re.findall(r'([\d]*)&nbsp;nights', total_price_str)[0])
            if command == 'lowprice' or command == 'highprice':
                total_price = int(re.findall(r'[$][0-9,]*\s', total_price_str)[0][1:-1].replace(',', ''))
                day_price = round(total_price / nights, 0)
            elif command == 'bestdeal':
                day_price = int(i_hotel['ratePlan']['price']['current'][1:])
                total_price = day_price * nights
            hotel_dict['price'] = day_price
            hotel_dict['total_price'] = total_price
            hotel_dict['coordinate'] = i_hotel['coordinate']
            hotel_dict['Thumb'] = i_hotel['optimizedThumbUrls']['srpDesktop']
            link = 'https://www.hotels.com/ho{}/?q-check-in={}&q-check-out={}&q-rooms=1&q-room-0-adults={}&q-room-0-children={}'.format(
                i_hotel['id'],
                checkin,
                checkout,
                adults,
                data.get('child', '0')
            )
            if childlist != 0:
                for i_child in range(len(childlist)):
                    child_text = '&q-room-0-child-{}-age={}'.format(i_child, childlist[i_child])
                    link += child_text
            hotel_dict['link'] = link
            dist_txt = i_hotel['landmarks'][0].get('distance')
            if dist_txt is not None:
                dist_km = round(float(re.findall(r'([\d.]*) mile', dist_txt)[0]), 1)
            else:
                dist_km = 5000
            hotel_dict['landmarks'] = dist_km
            hotel_list.append(hotel_dict)
        # print(hotel_list)
        if command == 'lowprice':
            hotel_list.sort(key=lambda x: x['price'])
        elif command == 'highprice':
            hotel_list.sort(key=lambda x: x['price'], reverse=True)
        elif command == 'bestdeal':
            # print(hotel_list)
            for i_hotel in hotel_list:
                bestdeal_weight = i_hotel['price'] * i_hotel['landmarks']
                i_hotel['bestdeal_weight'] = bestdeal_weight
                if i_hotel['landmarks'] > distance:
                    hotel_list.remove(i_hotel)
            hotel_list.sort(key=lambda x: x['bestdeal_weight'])

        return hotel_list
    except:
        return [0]
