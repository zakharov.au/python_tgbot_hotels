from config_data.config import RAPID_API_KEY
import aiohttp


async def city_search(city):
    try:
        url = "https://hotels4.p.rapidapi.com/locations/v2/search"
        querystring = {"query": city, "locale": "ru_RU"}
        headers = {
            "X-RapidAPI-Key": RAPID_API_KEY,
            "X-RapidAPI-Host": "hotels4.p.rapidapi.com"}
        # response = requests.request("GET", url, headers=headers, params=querystring)
        # data = json.loads(response.text)
        async with aiohttp.ClientSession() as session:
            async with session.get(url=url, headers=headers, params=querystring) as response:
                data: object = await response.json()
        cities = data['suggestions'][0]['entities']
        if len(cities) > 0:
            city_dict = dict()
            num = 1
            for i_city in cities:
                city_dict[num] = (i_city['name'], i_city['destinationId'])
                num += 1
            return True, city_dict
        else:
            return False, {}
    except:
        return False, {}


#a, b = city_founding('ыщпшокуом')
#print(a, b)
