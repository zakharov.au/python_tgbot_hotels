from aiogram import types
from config_data.config import DEFAULT_COMMANDS


async def set_default_commands(dp):
    await dp.bot.set_my_commands(
        [types.BotCommand(*i) for i in DEFAULT_COMMANDS]
    )
