import os
from dotenv import load_dotenv, find_dotenv
from aiogram import types

if not find_dotenv():
    exit('Переменные окружения не загружены т.к отсутствует файл .env')
else:
    load_dotenv()

BOT_TOKEN = os.getenv('BOT_TOKEN')
RAPID_API_KEY = os.getenv('RAPID_API_KEY')

DEFAULT_COMMANDS = [
    types.BotCommand('start', "Запустить бота"),
    types.BotCommand('help', "Вывести справку"),
    types.BotCommand('lowprice', 'Поиск дешевых отелей'),
    types.BotCommand('highprice', 'Поиск дорогих отелей'),
    types.BotCommand('bestdeal', 'Наиболее подходящие по цене и расположению отели'),
    types.BotCommand('history', 'История поиска')
]
