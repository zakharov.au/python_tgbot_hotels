**Hotel search Telegram bot**

Bot for searching hotels around the world.

**Commands**

/start

Used to start bot

/help

Sends the user describing commands bot.

/lowprice
Requests a city to search for hotels with accommodation options, offers city options, hotels with photos.
Shows hotels in ascending order of price.

/highprice

Requests a city to search for hotels with accommodation options, offers city options, hotels with photos.
Shows hotels in descending order of price.

/bestdeal

Requests a city to search for hotels with accommodation options, minimum and maximum cost, maximum distance from the city center, offers city options, hotels with photos.
Shows hotels that match the search conditions.

/history

Search history with search date and time,  hotels and photos.


**Requirements**

python-dotenv 0.20.0

pip 22.2.1

Pillow 9.2.0

Certify 2022.6.15

Ujson 1.33

Requests 2.28.1

Six 1.8.0

urllib3 1.26.11

setuptools 58.1.0

aiogram 2.22.1

aiohttp 3.8.1



**Installation instructions**

1.	Create your bot using the bot @BotFather and save your token.
2.	Register in the rapidapi.com/apidojo/api/hotels4/ and and save your token.
3.	Create a file .env in the program directory and fill it in according to .env.template
4.	Run main.py
