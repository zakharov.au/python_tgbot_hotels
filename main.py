from loader import bot, dp
from aiogram.utils import executor
from handlers.default_heandlers import start, echo
from handlers import lowprice, highprice, bestdeal, history, help
from database.database import db_start


async def on_startup(_):
    print('Bot started')
    db_start()

start.register_handlers_start(dp)
lowprice.register_handlers_lowprice_start(dp)
highprice.register_handlers_highrice_start(dp)
bestdeal.register_handlers_bestdeal_start(dp)
history.register_handlers_history_start(dp)
help.register_handlers_help_start(dp)
echo.register_handlers_echo(dp)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
